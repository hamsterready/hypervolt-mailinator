import sbt._

object Dependencies {
  lazy val zioTest = Seq(
    "dev.zio" %% "zio-test" % "2.0.2" % Test,
    "dev.zio" %% "zio-test-sbt" % "2.0.2" % Test,
    "dev.zio" %% "zio-test-magnolia" % "2.0.2" % Test
  )
  lazy val tapirZio = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server" % "1.1.2",
    "com.softwaremill.sttp.tapir" %% "tapir-json-zio" % "1.1.2",
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % "1.1.2"
  )
  lazy val zioJson = Seq(
    "dev.zio" %% "zio-json" % "0.3.0-RC11"
  )
  lazy val zioConfig = Seq(
    "dev.zio" %% "zio-config" % "3.0.2",
    "dev.zio" %% "zio-config-magnolia" % "3.0.2"
  )
  lazy val zioLogging = Seq(
    "dev.zio" %% "zio-logging-slf4j" % "2.1.1",
    "ch.qos.logback" % "logback-classic" % "1.4.3"
  )
}
