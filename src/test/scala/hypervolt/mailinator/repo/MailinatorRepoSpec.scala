package hypervolt.mailinator.repo

import hypervolt.mailinator.repo.Mailinator.Mail
import zio.ZIO
import zio.test._

object MailinatorRepoSpec extends ZIOSpecDefault {

  override def spec: Spec[Any, Nothing] = suite("MailinatorRepoSpec")(
    test("allows to add and retrieve emails") {
      for {
        s <- ZIO.service[MailinatorRepo]
        address = "to0@gmail.com"
        mail = Mail(id = "id1", from = "from@gmail.com", subject = "subject 1", body = "body 1")
        _ <- s.addMail(address, mail)
        copy <- s.getMail(address, mail.id)
      } yield assertTrue(mail == copy.get)
    },
    test("allows to retrieve non-existing emails") {
      for {
        s <- ZIO.service[MailinatorRepo]
        address = "to1@gmail.com"
        copy <- s.getMail(address, "I do not exists")
      } yield assertTrue(copy.isEmpty)
    },
    test("allows to add multiple emails and retrieve list of emails") {
      for {
        s <- ZIO.service[MailinatorRepo]
        address = "to2@gmail.com"
        mail1 = Mail(id = "id1", from = "from1@gmail.com", subject = "subject 1", body = "body 1")
        mail2 = Mail(id = "id2", from = "from2@gmail.com", subject = "subject 2", body = "body 2")
        _ <- s.addMail(address, mail1)
        _ <- s.addMail(address, mail2)
        list <- s.getMails(address)
      } yield assertTrue(List(mail1, mail2) == list)
    },
    test("allows to delete inbox") {
      for {
        s <- ZIO.service[MailinatorRepo]
        address = "to3@gmail.com"
        mail1 = Mail(id = "id1", from = "from1@gmail.com", subject = "subject 1", body = "body 1")
        mail2 = Mail(id = "id2", from = "from2@gmail.com", subject = "subject 2", body = "body 2")
        _ <- s.addMail(address, mail1)
        _ <- s.addMail(address, mail2)
        _ <- s.deleteMailbox(address)
        list <- s.getMails(address)
      } yield assertTrue(list.isEmpty)
    },
    test("allows to delete single mail") {
      for {
        s <- ZIO.service[MailinatorRepo]
        address = "to4@gmail.com"
        mail1 = Mail(id = "id1", from = "from1@gmail.com", subject = "subject 1", body = "body 1")
        mail2 = Mail(id = "id2", from = "from2@gmail.com", subject = "subject 2", body = "body 2")
        _ <- s.addMail(address, mail1)
        _ <- s.addMail(address, mail2)
        _ <- s.deleteMail(address, mail1.id)
        list <- s.getMails(address)
      } yield assertTrue(List(mail2) == list)
    }
    // TODO add concurrency tests, make sure that the underlying implementation is fiber-safe
    // see zio TMap tests, how are they implemented
  ).provide(MailinatorRepo.inMemoryRepo)
}
