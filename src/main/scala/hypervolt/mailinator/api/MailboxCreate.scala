package hypervolt.mailinator.api

import hypervolt.mailinator.repo.MailinatorRepo
import sttp.tapir.PublicEndpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import zio._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}
import sttp.tapir.ztapir.ZServerEndpoint
import sttp.tapir.ztapir._
import sttp.tapir.json.zio._

trait MailboxCreate {
  case class CreateRandomMailboxResponse(address: String)

  private implicit val decoder: JsonDecoder[CreateRandomMailboxResponse] = DeriveJsonDecoder.gen[CreateRandomMailboxResponse]
  private implicit val encoder: JsonEncoder[CreateRandomMailboxResponse] = DeriveJsonEncoder.gen[CreateRandomMailboxResponse]

  /** POST /mailboxes: Create a new, random email address.
    */
  private val createRandomMailbox: PublicEndpoint[Unit, Unit, CreateRandomMailboxResponse, Any] =
    endpoint.post
      .in("mailboxes")
      .out(jsonBody[CreateRandomMailboxResponse])

  private val createRandomMailboxLogic: Unit => ZIO[MailinatorRepo, Nothing, CreateRandomMailboxResponse] = _ =>
    for {
      username <- zio.Random.nextUUID
      address = s"$username@mailinator.hypervolt.co.uk" // TODO move to config
      _ <- ZIO.serviceWithZIO[MailinatorRepo](_.addAddress(address))
    } yield CreateRandomMailboxResponse(address)

  val createRandomMailboxEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    createRandomMailbox.zServerLogic(createRandomMailboxLogic)
}
