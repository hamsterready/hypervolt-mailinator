package hypervolt.mailinator.api

import hypervolt.mailinator.repo.Mailinator.{Address, Mail, MessageId}
import hypervolt.mailinator.repo.MailinatorRepo
import sttp.tapir.Endpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.zio._
import sttp.tapir.ztapir._
import zio._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

trait MailGet {
  case class MailGetResponse(mail: Option[Mail])

  private implicit val mailDecoder: JsonDecoder[Mail] = DeriveJsonDecoder.gen[Mail]
  private implicit val mailEncoder: JsonEncoder[Mail] = DeriveJsonEncoder.gen[Mail]

  private implicit val resDecoder: JsonDecoder[MailGetResponse] = DeriveJsonDecoder.gen[MailGetResponse]
  private implicit val resEncoder: JsonEncoder[MailGetResponse] = DeriveJsonEncoder.gen[MailGetResponse]

  /** GET /mailboxes/{email address}/messages/{message id}: Retrieve a specific message by id.
    */
  private val getMail: Endpoint[Unit, (Address, MessageId), Unit, MailGetResponse, Any] =
    endpoint.get
      .in("mailboxes" / path[Address]("address") / "messages" / path[MessageId]("mailId"))
      .out(jsonBody[MailGetResponse])

  private def getMailLogic(address: Address, messageId: MessageId): ZIO[MailinatorRepo, Nothing, MailGetResponse] = {
    // TODO map None to 404
    ZIO.serviceWithZIO[MailinatorRepo](_.getMail(address, messageId)).map(MailGetResponse)
  }

  val getMailEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    getMail.zServerLogic((getMailLogic _).tupled)
}
