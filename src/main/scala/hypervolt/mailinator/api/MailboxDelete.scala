package hypervolt.mailinator.api

import hypervolt.mailinator.repo.Mailinator.Address
import hypervolt.mailinator.repo.MailinatorRepo
import sttp.tapir.PublicEndpoint
import sttp.tapir.ztapir._
import zio._

trait MailboxDelete {

  /** DELETE /mailboxes/{email address}: Delete a specific email address and any associated messages.
    */
  private val deleteMailbox: PublicEndpoint[Address, Unit, Unit, Any] =
    endpoint.delete
      .in("mailboxes" / path[Address]("address") / "messages")

  private def deleteMailboxLogic(address: Address): ZIO[MailinatorRepo, Nothing, Unit] = {
    ZIO.serviceWithZIO[MailinatorRepo](_.deleteMailbox(address))
  }

  val deleteMailboxEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    deleteMailbox.zServerLogic(deleteMailboxLogic)
}
