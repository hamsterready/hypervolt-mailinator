package hypervolt.mailinator.api

import hypervolt.mailinator.repo.Mailinator.{Address, MessageId}
import hypervolt.mailinator.repo.MailinatorRepo
import sttp.tapir.Endpoint
import sttp.tapir.ztapir._
import zio._

trait MailDelete {

  /** DELETE /mailboxes/{email address}: Delete a specific email address and any associated messages.
    */
  private val deleteMail: Endpoint[Unit, (Address, MessageId), Unit, Unit, Any] =
    endpoint.delete
      .in("mailboxes" / path[Address]("address") / "messages" / path[MessageId]("messageId"))

  private def deleteMailLogic(address: Address, messageId: MessageId): ZIO[MailinatorRepo, Nothing, Unit] = {
    ZIO.serviceWithZIO[MailinatorRepo](_.deleteMail(address, messageId))
  }

  val deleteMailEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    deleteMail.zServerLogic((deleteMailLogic _).tupled)
}
