package hypervolt.mailinator.api

import hypervolt.mailinator.repo.MailinatorRepo
import hypervolt.mailinator.repo.Mailinator.{Address, Mail}
import sttp.tapir.PublicEndpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.zio._
import sttp.tapir.ztapir._
import zio._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

trait MailCreate {
  case class CreateMailRequest(from: Address, subject: String, body: String)

  case class CreateMailResponse(messageId: String)

  private implicit val reqDecoder: JsonDecoder[CreateMailRequest] = DeriveJsonDecoder.gen[CreateMailRequest]
  private implicit val reqEncoder: JsonEncoder[CreateMailRequest] = DeriveJsonEncoder.gen[CreateMailRequest]

  private implicit val resDecoder: JsonDecoder[CreateMailResponse] = DeriveJsonDecoder.gen[CreateMailResponse]
  private implicit val resEncoder: JsonEncoder[CreateMailResponse] = DeriveJsonEncoder.gen[CreateMailResponse]

  /** POST /mailboxes/{email address}/messages: Create a new message for a specific email address.
    */
  private val createMail: PublicEndpoint[(Address, CreateMailRequest), Unit, CreateMailResponse, Any] =
    endpoint.post
      .in("mailboxes" / path[Address]("address") / "messages")
      .in(jsonBody[CreateMailRequest])
      .out(jsonBody[CreateMailResponse])

  private def createMailLogic(address: Address, req: CreateMailRequest): ZIO[MailinatorRepo, Nothing, CreateMailResponse] = {
    for {
      messageId <- Random.nextUUID.map(_.toString)
      _ <- ZIO.serviceWithZIO[MailinatorRepo](_.addMail(address, Mail(messageId, req.from, req.subject, req.body)))
    } yield CreateMailResponse(messageId)
  }

  val createMailEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    createMail.zServerLogic((createMailLogic _).tupled)
}
