package hypervolt.mailinator.api

import hypervolt.mailinator.repo.MailinatorRepo
import sttp.apispec.openapi.circe.yaml._
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.swagger.SwaggerUI
import sttp.tapir.ztapir.ZServerEndpoint
import zhttp.http.{Http, Request, Response}

/** All the HTTP endpoints together with OpenAPI and Swagger UI.
  */
object HttpApp extends MailboxCreate with MailCreate with MailboxGet with MailGet with MailboxDelete with MailDelete {

  type Deps = MailinatorRepo

  /** All endpoints as Http
    */
  private val apiEndpoints: List[ZServerEndpoint[Deps, Any]] =
    List(createRandomMailboxEndpoint, createMailEndpoint, getMailboxEndpoint, getMailEndpoint, deleteMailboxEndpoint, deleteMailEndpoint)

  /** OpenAPI docs and Swagger UI
    */
  private val docsAsYaml =
    OpenAPIDocsInterpreter().toOpenAPI(apiEndpoints.map(_.endpoint), "Hypervolt Mailinator", "0.0.1-SNAPSHOT").toYaml
  private val swaggerUi: List[ZServerEndpoint[Deps, Any]] = SwaggerUI.apply(docsAsYaml)

  /** HTTP Application with all the endpoints.
    */
  val app: Http[Deps, Throwable, Request, Response] = ZioHttpInterpreter().toHttp(apiEndpoints ++ swaggerUi)
}
