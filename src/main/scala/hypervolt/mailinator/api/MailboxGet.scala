package hypervolt.mailinator.api

import hypervolt.mailinator.repo.MailinatorRepo
import hypervolt.mailinator.repo.Mailinator.{Address, Mail}
import sttp.tapir.PublicEndpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.zio._
import sttp.tapir.ztapir._
import zio._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

trait MailboxGet {
  case class MailboxGetResponse(mails: List[Mail])

  private implicit val mailDecoder: JsonDecoder[Mail] = DeriveJsonDecoder.gen[Mail]
  private implicit val mailEncoder: JsonEncoder[Mail] = DeriveJsonEncoder.gen[Mail]

  private implicit val resDecoder: JsonDecoder[MailboxGetResponse] = DeriveJsonDecoder.gen[MailboxGetResponse]
  private implicit val resEncoder: JsonEncoder[MailboxGetResponse] = DeriveJsonEncoder.gen[MailboxGetResponse]

  /** GET /mailboxes/{email address}/messages: Retrieve an index of messages sent to an email address,
    *                                          including sender, subject, and id, in recency order.
    *                                          Support cursor-based pagination through the index.
    */
  private val getMailbox: PublicEndpoint[Address, Unit, MailboxGetResponse, Any] =
    endpoint.get
      .in("mailboxes" / path[Address]("address") / "messages")
      .out(jsonBody[MailboxGetResponse])

  private def getMailboxLogic(address: Address): ZIO[MailinatorRepo, Nothing, MailboxGetResponse] = {
    ZIO.serviceWithZIO[MailinatorRepo](_.getMails(address).map(MailboxGetResponse))
  }

  val getMailboxEndpoint: ZServerEndpoint[MailinatorRepo, Any] =
    getMailbox.zServerLogic(getMailboxLogic)
}
