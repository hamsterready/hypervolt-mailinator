package hypervolt.mailinator

import hypervolt.mailinator.api.HttpApp
import hypervolt.mailinator.repo.MailinatorRepo
import zhttp.service.Server
import zio._
import zio.logging.backend.SLF4J

object Main extends ZIOAppDefault {
  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] =
    (for {
      // Logging configuration
      _ <- (Runtime.removeDefaultLoggers >>> SLF4J.slf4j).toRuntime
      port = 8080
      _ <- ZIO.logInfo(s"Starting server at $port")
      // Expose HTTP endpoints
      f1 <- Server.start(port, HttpApp.app).unit.fork
      // Start inbox cleaner - deletes first message from each inbox every two minutes
      f2 <- MailinatorRepo.scheduleEviction.fork
      // Join fibers after they both complete (hopefully never)
      _ <- (f1 <*> f2).join
    } yield ()).provide(MailinatorRepo.inMemoryRepo, Scope.default).exitCode
}
