package hypervolt.mailinator.repo

import hypervolt.mailinator.repo.Mailinator._
import zio.stm.TMap
import zio.{Schedule, UIO, ZIO, ZLayer, durationInt}

object Mailinator {
  // TODO we should use refined here
  type Address = String
  type MessageId = String

  /** Mail model
    * @param id the identifier of the message
    * @param from the address of mail sender
    * @param subject the subject of the email
    * @param body the content of the email
    */
  case class Mail(id: MessageId, from: Address, subject: String, body: String)
}

trait MailinatorRepo {

  /** Delete single mail from single mailbox
    *
    * @param address the address of the mailbox
    * @param messageId the identifier of the message to delete
    */
  def deleteMail(address: Address, messageId: MessageId): UIO[Unit]

  /** Drops whole mailbox.
    *
    * @param address the address of the mailbox to drop
    */
  def deleteMailbox(address: Address): UIO[Unit]

  /** Retrieve single email.
    *
    * @param address the address of the mailbox from which mail will be retrieved
    * @param messageId the identifier of the message to retrieve
    */
  def getMail(address: Address, messageId: MessageId): UIO[Option[Mail]]

  /** Creates new mailbox, not required to operate correctly.
    *
    * @param address the address of the mailbox to create
    */
  def addAddress(address: Address): UIO[Unit]

  /** Add mail to the mailbox.
    *
    * @param address the address of the mailbox to which email will be added
    * @param mail the email to be added to the inbox
    */
  def addMail(address: Address, mail: Mail): UIO[Unit]

  /** Retrieve all mails by address.
    *
    * @param address the address of the mailbox
    */
  def getMails(address: Address): UIO[List[Mail]]

  /** Evict old emails from all inboxes.
    */
  def evict(): UIO[Unit]
}

/** In memory structure that uses ZIO concurrency primitives like Ref or TMap
  *
  *  See [[https://zio.dev/reference/stm/ https://zio.dev/reference/stm/]]
  *
  * @param inboxes the tmap of all mailboxes
  */
case class InMemoryRepo(
    inboxes: TMap[String, List[Mail]]
) extends MailinatorRepo {

  override def addAddress(address: Address): UIO[Unit] =
    inboxes.putIfAbsent(address, List.empty[Mail]).commit.unit

  override def addMail(address: Address, mail: Mail): UIO[Unit] =
    inboxes.merge(address, List(mail))((xs, x) => xs ++ x).commit.unit

  override def getMails(address: Address): UIO[List[Mail]] =
    inboxes.getOrElse(address, List.empty).commit

  override def getMail(address: Address, messageId: MessageId): UIO[Option[Mail]] =
    inboxes.getOrElse(address, List.empty).commit.map(_.find(_.id == messageId))

  override def deleteMailbox(address: Address): UIO[Unit] =
    inboxes.delete(address).commit

  override def deleteMail(address: Address, messageId: MessageId): UIO[Unit] =
    inboxes.updateWith(address)(x => x.map(_.filterNot(_.id == messageId))).commit.unit

  override def evict(): UIO[Unit] = {
    // this is artificial implementation, it will remove head from each inbox
    inboxes.transformValues(_.tail).commit
  }
}

object MailinatorRepo {

  /** Simple in memory implementation based on ZIO STM primitives.
    */
  val inMemoryRepo: ZLayer[Any, Nothing, MailinatorRepo] = ZLayer {
    for {
      mailboxes <- TMap.empty[Address, List[Mail]].commit
    } yield InMemoryRepo(mailboxes)
  }

  /** Run evictio every some time.
    */
  def scheduleEviction: ZIO[MailinatorRepo, Nothing, Unit] = {
    val everySometime = Schedule.fixed(2.minutes)
    ZIO.serviceWithZIO[MailinatorRepo](_.evict()).repeat(everySometime).unit
  }
}
