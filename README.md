# Hypervolt Mailinator

Detailed requirements can be found [here](https://github.com/fauna/exercises/blob/main/backend.md).

# How to run?

If you have `sbt` installed, and you trust this code:

```sbt
sbt run
```

If you want docker-based run, inspect [Dockerfile](./Dockerfile), and use:

```shell
docker build . -t maciej-mailinator
docker run -p 8080:8080 maciej-mailinator
```

# How to inspect and use?
Visit [swagger endpoint](http://localhost:8080/docs).

# Implementation Notes

This implementation is using ZIO framework. To expose HTTP endpoint with OpenAPI and Swagger interface we use Tapir.

This implementation uses non-persistent, in-memory structure which is guarded by [ZIO Software Transactional Memrory](https://zio.dev/reference/stm/).

## Missing bits
Lack of tests to prove that the concurrent access is actually correct.

# Original email

> Please do addd a read.me with instructions as to what you would do with more time, choices made etc.
>
> Introduction
>
> We really do not want you to spend more than 2-3 hours tops on this test and we have worked hard to use something and set out instructions to make this a realistic expectation.
>
>
>
> We are looking for the following and in this order of prioritisation:
>
> 1. Correct & Readable
>
> 2. Sensible Algorithm Choices
>
> 3. Performance
>
> And a Read.Me (further information on what we are looking for there is found in the paragraph below)
>
>
>
> You are completely free to choose how you tackle these aspects and we are looking for you to solve the problem in the best way you feel you know how
>
> Please include a Read.me
>
> We are not looking to nit pick, snipe or for you to spend hours and hours on this.
>
> Please simply use the Read.me to explain to us the design choices you've made and the things you didn't have time for, simply explain why you chose not to tackle it and what you would do with more time
>
>
>
> The Tech Test
>
> https://github.com/fauna/exercises/blob/main/backend.md
>
>
>
> Extra Credit
>
> Our stack is Scala 2.0, Finagle, Flink, GitLab, Terraform, DynamoDB, AWS,
>
> Bonus points for attempting to use Fingale but do keep in mind that we value correct and readable code above all else for this exercise
>
> 